/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.paywholesail.assistedinject;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes= {
    NeedsAssistanceFactoryConfiguration.class,
    AssistedInjectTest.AssistanceConfiguration.class,
})
public class AssistedInjectTest {
    private static final String EXPECTED_STR = "Platypus";
    private static final int EXPECTED_INT = 1337;

    @Inject
    Clock expectedClock;

    @Inject
    NeedsAssistanceFactory factory;

    @Test
    public void assistedInjectionWorks() {
        final int param = 42;
        assertThat(factory.createDefault(param, 6))
                .isNotNull()
                .extracting(NeedsAssistance::getClock, NeedsAssistance::getNamedString, NeedsAssistance::getCustomQualifiedInt, NeedsAssistance::getParameter)
                .containsExactly(expectedClock, EXPECTED_STR, EXPECTED_INT, param);
    }

    @Configuration
    static class AssistanceConfiguration {
        @Bean
        Clock clock() {
            return Clock.fixed(Instant.ofEpochMilli(123456789), ZoneOffset.UTC);
        }

        @Bean
        int unqualifiedInt() {
            return 0;
        }

        @Bean
        @CustomQualifier
        int customQualifiedInt() {
            return EXPECTED_INT;
        }

        @Bean
        String unnamedString() {
            return "Your platypus is in another castle.";
        }

        @Bean
        @Named("named")
        String namedString() {
            return EXPECTED_STR;
        }
    }
}
