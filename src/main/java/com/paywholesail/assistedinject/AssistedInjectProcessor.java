/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.paywholesail.assistedinject;

import java.io.IOException;
import java.io.Writer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Generated;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;
import jakarta.inject.Inject;

@SupportedAnnotationTypes("com.paywholesail.assistedinject.AssistedInject")
public class AssistedInjectProcessor extends AbstractProcessor {
    private static final Set<ElementKind> ACCEPTABLE = EnumSet.of(ElementKind.CLASS, ElementKind.INTERFACE);

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnv) {
        if (annotations.isEmpty()) {
            return false;
        }
        roundEnv.getElementsAnnotatedWith(annotations.iterator().next())
                .forEach(new Generator()::safeGenerate);
        return true;
    }

    class Generator {
        private long counter = 0;
        private void safeGenerate(final Element factory) {
            try {
                generate(factory);
            } catch (final Exception ex) {
                processingEnv.getMessager().printMessage(Kind.ERROR, "Failure: " + ex, factory);
                return;
            }
        }

        private void generate(final Element factoryE) throws IOException {
            final String pkg = processingEnv.getElementUtils().getPackageOf(factoryE).getQualifiedName().toString();
            processingEnv.getMessager().printMessage(Kind.NOTE,
                    String.format("[assistedinject] generating factory implementation of %s in package '%s'", factoryE, pkg));
            if (!ACCEPTABLE.contains(factoryE.getKind())) {
                error(factoryE, "AssistedInject on non-class");
                return;
            }
            if (!factoryE.getModifiers().contains(Modifier.ABSTRACT)) {
                error(factoryE, "AssistedInject on non-abstract class");
            }

            final TypeElement factory = (TypeElement) factoryE;
            final String implName = factory.getSimpleName() + "Impl";
            final TypeSpec.Builder implSpec = TypeSpec.classBuilder(implName);
            implSpec.addAnnotation(generated());
            final TypeName superName = TypeName.get(factory.asType());
            addSuper(implSpec, factory, superName);


            final List<ExecutableElement> factoryMethods = factory.getEnclosedElements()
                    .stream()
                    .filter(ee -> ee.getKind() == ElementKind.METHOD)
                    .map(ExecutableElement.class::cast)
                    .filter(ee -> !ee.getModifiers().contains(Modifier.PRIVATE))
                    .filter(ee -> !ee.getModifiers().contains(Modifier.DEFAULT))
                    .collect(Collectors.toCollection(ArrayList::new));

            final Map<Injectable, String> injectableNames = new LinkedHashMap<>();

            factoryMethods.stream()
                       .map(ee -> generateMethod(injectableNames, ee))
                       .flatMap(o -> o.map(Stream::of).orElse(Stream.empty()))
                       .forEach(implSpec::addMethod);

            final MethodSpec.Builder constructor = MethodSpec.constructorBuilder()
                    .addAnnotation(Inject.class);
            final CodeBlock.Builder constructorImpl = CodeBlock.builder();
            injectableNames.forEach((injectable, name) -> {
                constructor.addParameter(ParameterSpec.builder(injectable.type, name)
                        .addAnnotations(injectable.annotations.stream()
                                .map(AnnotationSpec::get)
                                .collect(Collectors.toList()))
                        .build());
                implSpec.addField(injectable.type, name, Modifier.PRIVATE, Modifier.FINAL);
                constructorImpl.add("this.$L = $L;\n",
                        name,
                        name);
            });
            implSpec.addMethod(
                    constructor.addCode(constructorImpl.build())
                               .build());

            final JavaFileObject file = processingEnv.getFiler().createSourceFile(pkg + "." + implName, factoryE);
            final TypeSpec implClass = implSpec.build();
            try (Writer out = file.openWriter()) {
                JavaFile.builder(pkg, implClass).indent("    ")
                    .build().writeTo(out);
            }

            final TypeElement configAnnotation = processingEnv.getElementUtils().getTypeElement("org.springframework.context.annotation.Configuration");
            if (configAnnotation != null) {
                final String configClassName = factory.getSimpleName() + "Configuration";
                final TypeSpec configClass = generateSpringConfiguration(ClassName.get(configAnnotation), ClassName.get(pkg, implName), TypeSpec.classBuilder(configClassName));
                final JavaFileObject configFile = processingEnv.getFiler().createSourceFile(pkg + "." + configClassName, factoryE);
                try (Writer out = configFile.openWriter()) {
                    JavaFile.builder(pkg, configClass).indent("    ")
                        .build().writeTo(out);
                }
            }
        }

        private TypeSpec generateSpringConfiguration(final ClassName configAnnotation, final ClassName implClass, final Builder configClassBuilder) {
            configClassBuilder.addAnnotation(generated());
            configClassBuilder.addModifiers(Modifier.PUBLIC);
            configClassBuilder.addAnnotation(configAnnotation);
            configClassBuilder.addAnnotation(AnnotationSpec.builder(ClassName.get(configAnnotation.packageName(), "Import"))
                    .addMember("value", "$T.class", implClass)
                    .build());
            return configClassBuilder.build();
        }

        private Optional<MethodSpec> generateMethod(final Map<Injectable, String> injectableNames, final Element el) {
            final ExecutableElement method = (ExecutableElement) el;
            final List<? extends VariableElement> methodParams = method.getParameters();
            final TypeElement returnTypeElt = processingEnv.getElementUtils()
                    .getTypeElement(method.getReturnType().toString());
            if (returnTypeElt == null) {
                error(el, "AssistedInject: could not process return type %s", method.getReturnType());
                return Optional.empty();
            }
            final Optional<MethodSpec> result = returnTypeElt
                    .getEnclosedElements()
                    .stream()
                    .filter(e -> ElementKind.CONSTRUCTOR.equals(e.getKind()))
                    .map(ExecutableElement.class::cast)
                    .filter(e -> matchAssistedInject(e.getParameters(), methodParams))
                    .findFirst()
                    .map(constructor -> generateMethod(injectableNames, method, constructor));
            if (!result.isPresent()) {
                error(el, "AssistedInject: No constructor in %s has matching parameters (including name!) from factory method %s",
                        method.getReturnType(), method);
            }
            return result;
        }

        private MethodSpec generateMethod(final Map<Injectable, String> injectableNames, final ExecutableElement method, final ExecutableElement constructor) {
            final List<? extends VariableElement> constructorParams = constructor.getParameters();
            final ArrayList<String> invocationParams = new ArrayList<String>();
            for (final VariableElement param : constructorParams) {
                if (param.getAnnotation(AssistedParam.class) != null) {
                    invocationParams.add(param.getSimpleName().toString());
                } else {
                    final Injectable key = new Injectable(TypeName.get(param.asType()), Set.copyOf(param.getAnnotationMirrors()));
                    final String fieldName = injectableNames.computeIfAbsent(key, x -> param.getSimpleName() + "_" + counter++);
                    invocationParams.add("this." + fieldName);
                }
            }

            final String paramList = invocationParams.stream().collect(Collectors.joining(", "));

            final CodeBlock.Builder body = CodeBlock.builder()
                        .add("return new $T($L);\n",
                                method.getReturnType(),
                                paramList);

            return MethodSpec.overriding(method)
                    .addCode(body.build())
                    .build();
        }
    }

    private boolean matchAssistedInject(final List<? extends VariableElement> constructorParams, final List<? extends VariableElement> methodParams) {
        return paramKeys(constructorParams.stream()
                    .filter(ve -> ve.getAnnotation(AssistedParam.class) != null))
                .equals(paramKeys(methodParams.stream()));
    }

    private Set<Object> paramKeys(final Stream<? extends VariableElement> vars) {
        return vars
                .map(ve -> new AbstractMap.SimpleEntry<>(ve.getSimpleName(), ve.asType().toString()))
                .collect(Collectors.toSet());
    }

    private void addSuper(final TypeSpec.Builder spec, final TypeElement el, final TypeName superName) {
        if (el.getKind() == ElementKind.CLASS) {
            spec.superclass(superName);
        } else {
            spec.addSuperinterface(superName);
        }
    }

    private static AnnotationSpec generated() {
        return AnnotationSpec.builder(Generated.class)
                .addMember("value", "$S", AssistedInjectProcessor.class.getName())
                .build();
    }

    record Injectable (TypeName type, Set<AnnotationMirror> annotations) {}

    private void error(final Element elem, final String fmt, final Object... args) {
        processingEnv.getMessager().printMessage(Kind.ERROR, String.format(fmt, args), elem);
    }
}
