/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.paywholesail.assistedinject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import jakarta.inject.Inject;

/**
 * Annotate a {@code @AssistedInject} factory interface to generate a {@code jakarta.inject}-enabled implementation.
 *
 * Every implemented factory method will create a {@code new} instance of its return type.  The instance is created
 * by finding the constructor that has {@link AssistedParam}-annotated parameters with the same type and name as the
 * factory method.  Any constructor parameters that are not so annotated will be {@link Inject}ed in the factory constructor.
 */
@Target(ElementType.TYPE)
public @interface AssistedInject {}
