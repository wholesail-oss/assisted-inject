# assisted-inject

Easily generate boilerplate Factory implementations that combine
program-supplied arguments with IoC constructor injection.

## Use

A common problem when using Dependency Injection frameworks like
Spring or Guice is needing to accept some constructor parameters
while also accepting customization from the calling program.

This module is inspired by
[Guice's implementation](https://github.com/google/guice/wiki/AssistedInject)
and driven by the lack of an equivalent Spring solution.

To generate a most-excellent factory implementation for your own use,
start with a class that needs assistance:

```java
class Starship {
    Starship(WarpCore warpCore, Deflector deflector, Transporter transporter, ..., String name, List<Person> crew) {
        this.warpCore = warpCore;
        ....
        this.crew = List.copyOf(crew);
    }
}
```

Creating instances of this class is a huge pain!  You have to assemble the first set of parameters from your DI framework,
accept the `crew`, then finally construct the instance.  Let's have the compiler do the work for us.


Declare a factory interface:

```java
@AssistedInject
interface StarshipFactory {
    Starship create(String name, List<Person> crew);
}
```

Mark up the existing constructor to indicate which parameters are supplied by the factory:

```java
class Starship {
    Starship(WarpCore warpCore, ..., @AssistedParam String name, @AssistedParam List<Person> crew) {
        ....
    }
}
```

Please note, the signature (type, name, and `jakarta.inject.Qualifier` annotations) of parameters must
exactly match between the factory method and the constructor parameters marked `@AssistedParam` or a compile
error will result.

The source generator will manufacture a factory implementation for you that does all the bookkeeping:

```java
class StarshipFactoryImpl implements StarshipFactory {
    private final WarpCore warpCore;
    private final Deflector deflector;

    @Inject
    StarshipFactoryImpl(WarpCore warpCore, Deflector deflector, ...) {
        this.warpCore = warpCore;
        this.deflector = deflector;
        ...
    }

    @Override
    public Starship create(String name, int crew) {
        return new Starship(warpCore, deflector, ..., name, crew);
    }
}
```

If `spring-context` is on your compile classpath, the generator will also construct a
`StarshipFactoryConfiguration` class suitable for import or autodiscovery.

## Setup

Assisted-Inject is an annotation processor that plugs in to your Java compiler.
Our friends at Immutables have a [handy setup guide](https://immutables.github.io/apt.html)
that got me started.

## Dependencies

Assisted-Inject has no runtime dependency.  All annotation processing happens during compile,
additional source files are created with the excellent [JavaPoet](https://github.com/square/javapoet)
library, and the compiled sources and classes are available to the build same as any "normal" source file is.

Generated sources are intentionally kept tidy so an unlucky operator isn't stuck reading awful generated code-soup.

## Distribution

assisted-inject is distributed through [Maven Central](https://search.maven.org) thanks to [Sonatype OSSRH](https://oss.sonatype.org).  Thanks!

```xml
<dependency>
  <groupId>com.paywholesail</groupId>
  <artifactId>assisted-inject</artifactId>
  <version>1.0.0</version>
  <scope>provided</scope> <!-- Annotations only!  No runtime library needed. -->
</dependency>
```

## License

Copyright [Wholesail](https://paywholesail.com), released under the Apache-2.0 license.
